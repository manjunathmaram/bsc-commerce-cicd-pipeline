def call(commerceDir) {
    echo "##### Extract commerce platform ##### -- ${commerceDir}"
    sh "unzip -o ../CXCOMM2105*.ZIP -d ${commerceDir}/core-customize"
    /** Uncomment if you will be using the Integration Extension Pack**/
    echo "##### Extract commerce integration pack #####"
    sh "unzip -o ../CXCOMINTPK2108*.ZIP -d ${commerceDir}/core-customize"
    
}  